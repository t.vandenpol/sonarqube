########################################################################################################################
import math
import time
import matplotlib
import pandas as pd
import numpy as np
import pickle
import pathlib
import numpy
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import precision_recall_curve
from sklearn.cluster import MiniBatchKMeans
from sklearn.metrics import confusion_matrix
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.model_selection import StratifiedKFold
from numpy import asarray
from numpy import savetxt
from numpy import loadtxt
import matplotlib.pyplot as plt
import multiprocessing
from numpy import interp
from sklearn.metrics import roc_curve, auc, roc_auc_score
import Main.Code.settings as s
from sklearn.metrics import fbeta_score
from Main.Code.help_functions import make_sure_folder_exists, var_to_string, save_to_csv, is_file, f_beta

matplotlib.use("PDF")
num_cores = multiprocessing.cpu_count()


########################################################################################################################

def get_probabilities(clf, x, is_svm):
    if is_svm:
        return clf.decision_function(x)
    else:  # e.g. s.MODEL == 'RandomForest'
        return clf.predict_proba(x)[:, 1]

# Save for each fold the permutation probabilities
y_probas_perm = []
y_test_perm = []

def permutation_importance(clf, n_cols, x_test, y_test, is_svm, roc_auc_baseline, n_round):
    tp_added_perm = np.zeros(n_cols)
    tn_added_perm = np.zeros(n_cols)
    fp_added_perm = np.zeros(n_cols)
    fn_added_perm = np.zeros(n_cols)

    new_round_list_probas = []
    new_round_list_y_test = []

    roc_auc_added_perm = np.zeros(n_cols)
    for col in range(n_cols):
        # # Store column for restoring it later
        # save = x_test[:, col].copy()
        # Randomly shuffle all values in this column
        x_test[:, col] = np.random.permutation(x_test[:, col])
        # Compute TP FP FN TP after distortion
        y_prediction_perm = clf.predict(x_test)
        tn_perm, fp_perm, fn_perm, tp_perm = confusion_matrix(y_test, y_prediction_perm).ravel()
        tp_added_perm[col] += tp_perm
        tn_added_perm[col] += tn_perm
        fp_added_perm[col] += fp_perm
        fn_added_perm[col] += fn_perm
        probas = get_probabilities(clf, x_test, is_svm)
        new_round_list_probas.append(probas)
        new_round_list_y_test.append(y_test)
        # Compute AUC score after distortion
        roc_auc_score_permuted = roc_auc_score(y_test, probas)
        # # Restore the original data
        # x_test[:, col] = save
        # Importance is incremented by the drop in accuracy:
        roc_auc_added_perm[col] += (roc_auc_baseline - roc_auc_score_permuted)

    y_probas_perm.append(new_round_list_probas)
    y_test_perm.append(new_round_list_y_test)

    return tp_added_perm, tn_added_perm, fp_added_perm, fn_added_perm, roc_auc_added_perm


# Importance calculated dropping columns
def drop_column_importance(clf, n_cols, x_test, y_test, x_train, y_train, is_svm, roc_auc_baseline):
    # clf, p, x_test, y_train, y_test, is_svm, baseline, g_mean, f_bet
    tp_added_drop = np.zeros(n_cols)
    tn_added_drop = np.zeros(n_cols)
    fp_added_drop = np.zeros(n_cols)
    fn_added_drop = np.zeros(n_cols)
    roc_auc_added_drop = np.zeros(n_cols)
    for col in range(n_cols):
        # perform drop column
        # Drop a column at the time and fit the model
        x_train_drop = np.delete(x_train, np.s_[col], axis=1)
        x_test_drop = np.delete(x_test, np.s_[col], axis=1)
        # fit to overwrite the fitted model to obtain a model trained without the dropped column.
        clf.fit(x_train_drop, y_train)
        # Compute y after distortion
        y_prediction_drop = clf.predict(x_test_drop)
        tn_drop, fp_drop, fn_drop, tp_drop = confusion_matrix(y_test, y_prediction_drop).ravel()
        tp_added_drop[col] += tp_drop
        tn_added_drop[col] += tn_drop
        fp_added_drop[col] += fp_drop
        fn_added_drop[col] += fn_drop
        # Compute AUC score after distortion
        roc_auc_score_drop = roc_auc_score(y_test, get_probabilities(clf, x_test_drop, is_svm))
        # Importance is incremented by the drop in accuracy:
        roc_auc_added_drop[col] += (roc_auc_baseline - roc_auc_score_drop)
        # if (roc_auc_added_drop[col]<0):
        #     print(col)
        #     print(roc_auc_baseline)
        #     print(roc_auc_score_drop)
    return tp_added_drop, tn_added_drop, fp_added_drop, fn_added_drop, roc_auc_added_drop


def cross_validate(clf, x, y, column_names, ml_name, is_svm, filename):
    # Use stratified k fold cross-validation with shuffling.
    cv = StratifiedKFold(n_splits=s.n_folds, shuffle=True, random_state=0)
    # Set the seed to use for randomisation, for reproducibility
    np.random.seed(0)

    _, n_cols = x.shape  # get the dimensions of the data, we want the number of columns.

    # We add up TN FP FN TP for each feature at their respective index.
    # For AUC we want to average
    # For the other measures, we calculate once at the end on the confusion metrics
    # Add up TN FP FN TP for confusion matrix.
    # Do this for permutation and drop-column, when on.
    # We do add up AUC every fold and in the end we divide by the number of folds to get the average.
    tp_added_perm = np.zeros(n_cols)  # Permutation
    tn_added_perm = np.zeros(n_cols)
    fp_added_perm = np.zeros(n_cols)
    fn_added_perm = np.zeros(n_cols)
    roc_auc_added_perm = np.zeros(n_cols)

    tp_added_drop = np.zeros(n_cols)  # Drop-Column
    tn_added_drop = np.zeros(n_cols)
    fp_added_drop = np.zeros(n_cols)
    fn_added_drop = np.zeros(n_cols)
    roc_auc_added_drop = np.zeros(n_cols)

    tp_baseline = 0  # Baseline
    tn_baseline = 0
    fp_baseline = 0
    fn_baseline = 0
    roc_auc_baseline_added = 0

    # for Precision recall curve
    y_true_list = []
    probas_pred_list = []

    # for ROC curve plot
    tprs = []
    thresholds = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    plt.figure(400)  # figure 400 is ROC Curve
    plt.figure(figsize=(10, 10))

    n_round = 1  # The number of the current round
    for train, test in cv.split(x, y):
        start = time.time()  # Start measuring the time it takes to run this ML model this round
        print("Currently round %s" % str(n_round))
        filename_and_round = "%s-%s" % (filename, n_round)

        # -------------------- OVERSAMPLING ------------------------
        # Where to save the oversampled data for each fold, so that we can reuse this for other Machine Learning.
        data_folds_folder_x_train = "oversampled_data/%s_%s_%s" % ('Xtrain', s.one_project, n_round) + '.csv'
        data_folds_folder_y_train = "oversampled_data/%s_%s_%s" % ('ytrain', s.one_project, n_round) + '.csv'
        make_sure_folder_exists(data_folds_folder_x_train)
        used_save_k_data = 'no saved k_data used'  # No saved data is used by default.
        # When we use KMeansSMOTE, we use oversampled data
        if s.k_means_smote == 'on':
            # The data does exist and does not need to be overwritten. Load and use the data
            if is_file(data_folds_folder_x_train) and s.overwrite_k_save != 'on':
                used_save_k_data = '\n' + data_folds_folder_x_train + '\n' + data_folds_folder_y_train
                x_train = loadtxt(data_folds_folder_x_train, delimiter=',')
                y_train = loadtxt(data_folds_folder_y_train, delimiter=',')
            # The data does not exist or should be overwritten, generate and save it
            # Environment should be K1 to use oversampling. Use environment M3 to use AdaC2
            else:  # Create oversampled data
                from imblearn.over_sampling import KMeansSMOTE
                x_train = x[train, :]
                y_train = y[train]
                k_means_estimate = MiniBatchKMeans(random_state=0, n_clusters=4, batch_size=2048, verbose=1)
                kms = KMeansSMOTE(random_state=0, cluster_balance_threshold=0.001, kmeans_estimator=k_means_estimate)
                x_train, y_train = kms.fit_resample(x_train, y_train)
                data_x_train = asarray(x_train)
                data_y_train = asarray(y_train)
                savetxt(data_folds_folder_x_train, data_x_train, delimiter=',')
                savetxt(data_folds_folder_y_train, data_y_train, delimiter=',')
        else:  # Use normal data instead of oversampled data.
            x_train = x[train, :]
            y_train = y[train]

        # -------------------- TRAIN AND TEST DATA ------------------------
        # test data is always the same:
        x_test = x[test, :]
        y_test = y[test]

        # -------------------- TRAIN MODEL, OR USE TRAINED MODEL ------------------------
        # We use the name of the used filter string and the count, so that we can hopefully use it for other ML.
        clf_file_name = 'Saved_ML/%s_%s.pkl' % (filename, n_round)
        if s.model == 'SVM_slower':  # SVM_slower is different ML model.
            clf_file_name = 'Saved_ML/%s_%s.pkl' % (filename + ml_name, n_round)
        # Load the ML Model CLF if already run once and we do not want to overwrite it.
        used_save_ml_model = 'No ML saved ML used'
        if is_file(clf_file_name) and s.overwrite_saves != 'on':
            # Use saved ML if it is saved and does not need to be overwritten.
            used_save_ml_model = clf_file_name
            with open(clf_file_name, 'rb') as fML:
                clf = pickle.load(fML)
        else:
            # Train the model
            clf.fit(x_train, y_train)
            # Safe ML
            make_sure_folder_exists(clf_file_name)
            with open(clf_file_name, 'wb') as fML:
                pickle.dump(clf, fML)
        # -------------------- CALCULATE TP FP TN FN AND AUC FOR BASELINE ------------------------
        # Save Ytrue and Yprediction for precision recall curve
        y_true_list.extend(y_test)
        probas_baseline = get_probabilities(clf, x_test, is_svm)
        probas_pred_list.extend(probas_baseline)
        # ROC Curve:
        fpr, tpr, threshold = roc_curve(y_test, probas_baseline)
        tprs.append(interp(mean_fpr, fpr, tpr))  # interpolate the mean_fpr axis values
        thresholds.append(interp(mean_fpr, fpr, threshold))
        tprs[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='ROC fold %d (AUC = %0.2f)' % (n_round, roc_auc))
        # AUC:
        roc_auc_baseline = roc_auc_score(y_test, probas_baseline)
        roc_auc_baseline_added += roc_auc_baseline

        # Predict the labels for the test set
        y_pred_baseline = clf.predict(x_test)

        # Calculate TN FP FN TP
        tn_base, fp_base, fn_base, tp_base = confusion_matrix(y_test, y_pred_baseline).ravel()

        # Add TN FP FN TP for this round to the count.
        tp_baseline += tp_base
        fp_baseline += fp_base
        tn_baseline += tn_base
        fn_baseline += fn_base

        # save name for feature importance values in .csv

        # -------------------- PERMUTATION ------------------------
        # Only calculate the permutation importances when they are already calculated and we do not want to overwrite.
        if s.permutation == 'on':
            if (not is_file('./perm_raw/' + filename + '.data')) or (s.overwrite_saves == 'on'):
                tp_perm, tn_perm, fp_perm, fn_perm, roc_auc_perm= \
                    permutation_importance(clf, n_cols, x_test, y_test, is_svm, roc_auc_baseline, n_round)
                # Add the AUC together, so that in the end we can calculate the average AUC.
                roc_auc_added_perm += roc_auc_perm
                # Add the tp,tn,fp,fn together, so that in the end we can calculate and save the importances.
                tp_added_perm += tp_perm
                tn_added_perm += tn_perm
                fp_added_perm += fp_perm
                fn_added_perm += fn_perm

        # -------------------- DROP-COLUMN ------------------------
        # Only calculate the drop-column importances when they are already calculated and we do not want to overwrite.
        if s.drop_column == 'on':
            if (not is_file('./drop_raw/' + filename + '.data')) or (s.overwrite_saves == 'on'):
                tp_drop, tn_drop, fp_drop, fn_drop, roc_auc_drop = \
                    drop_column_importance(clf, n_cols, x_test, y_test, x_train, y_train, is_svm, roc_auc_baseline)
                # Add the AUC together, so that in the end we can calculate the average AUC.
                roc_auc_added_drop += roc_auc_drop
                # Add the tp,tn,fp,fn together, so that in the end we can calculate and save the importances.
                tp_added_drop += tp_drop
                tn_added_drop += tn_drop
                fp_added_drop += fp_drop
                fn_added_drop += fn_drop

        n_round += 1
        end = time.time()
        print('Round time: %s' % (end - start))
        print("\n")

    # Save AUC values average for single and multi-plot
    save_values = [ml_name, tprs, mean_fpr, aucs, thresholds]
    target_file = './aucs/'
    make_sure_folder_exists(target_file)
    with open(target_file + filename + '.data', 'wb') as fp:  # Pickling
        pickle.dump(save_values, fp)

    # Save Precision-Recall values average for single and multi-plot
    save_values = [ml_name, probas_pred_list, y_true_list]
    target_file = './precision_recall_curve/'
    make_sure_folder_exists(target_file)
    with open(target_file + filename + '.data', 'wb') as fp:  # Pickling
        pickle.dump(save_values, fp)

    # Save Baseline values etc:
    save_values = [ml_name, tp_baseline, fn_baseline, tn_baseline, fp_baseline, roc_auc_baseline_added]
    target_file = './baseline_raw/'
    make_sure_folder_exists(target_file)
    with open(target_file + filename + '.data', 'wb') as fp:  # Pickling
        pickle.dump(save_values, fp)

    if s.permutation == 'on':  # Save Permutation values
        if (not is_file('./perm_raw/' + filename + '.data')) or (s.overwrite_saves == 'on'):
            save_values = [ml_name, tp_added_perm, tn_added_perm, fp_added_perm, fn_added_perm, roc_auc_added_perm]
            target_file = './perm_raw/'
            make_sure_folder_exists(target_file)
            with open(target_file + filename + '.data', 'wb') as fp:  # Pickling
                pickle.dump(save_values, fp)
        if (not is_file('./probas_perm_raw/' + filename + '.data')) or (s.overwrite_saves == 'on'):
            save_values = [ml_name, y_probas_perm, y_test_perm, roc_auc_added_perm]
            target_file = './probas_perm_raw/'
            make_sure_folder_exists(target_file)
            with open(target_file + filename + '.data', 'wb') as fp:  # Pickling
                pickle.dump(save_values, fp)

    if s.drop_column == 'on':  # Save Drop_Column values
        if (not is_file('./drop_raw/' + filename + '.data')) or (s.overwrite_saves == 'on'):
            save_values = [ml_name, tp_added_drop, tn_added_drop, fp_added_drop, fn_added_drop, roc_auc_added_drop]
            target_file = './drop_raw/'
            make_sure_folder_exists(target_file)
            with open(target_file + filename + '.data', 'wb') as fp:  # Pickling
                pickle.dump(save_values, fp)

    print('Used ' + used_save_k_data)

    return
