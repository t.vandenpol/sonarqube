import os
import pandas as pd
import numpy as np
from pathlib import Path


def make_sure_folder_exists(path):
    folder = os.path.dirname(path)
    if not os.path.isdir(folder):
        os.makedirs(folder)


def var_to_string(var, space):
    return [name for name in space if space[name] is var][0]


def save_to_csv(feature_values, feature_names, folder_name, filename):
    idx = np.argsort(feature_values)  # Gives the indices that would sort this array in increasing order
    sorted_column_names = list(np.array(feature_names)[idx])  # Gives the column names in the corresponding sorted order

    # Make a dataframe with the sorted columns and values.
    importance_drop = pd.DataFrame({'Variables': sorted_column_names, 'Importance': feature_values[idx]})
    # Setup the file location
    target_file = folder_name + "/" + filename
    # Make sure the folder exist
    make_sure_folder_exists(target_file)
    # Create the .csv file
    importance_drop.to_csv(target_file + ".csv", columns=['Variables', 'Importance'], sep=';', index=False)


def is_file(filepath_and_name):
    filepath = Path(filepath_and_name)
    return filepath.is_file()


def f_beta(beta, precision, recall):
    if (precision == 0) or (recall == 0):
        return 0
    else:
        return (1 + beta * beta) * (precision * recall) / (beta * beta * precision + recall)


# def f_beta_array(beta, precision, recall):
#     if (precision == 0) or (recall == 0):
#         return 0
#     else:
#         return (1 + beta * beta) * (precision * recall) / (beta * beta * precision + recall)

# if s.model == 'SVM' or s.model == 'SVM_slower':  # normalize the data, when SVM is used. TODO: Test does this make it better?/faster, overleaf says no
#     scaler = StandardScaler()
#     x_train = scaler.fit_transform(x_train)
#     x_test = scaler.transform(x_test)

# save importances, gmean, fbeta permutation importance per feature in .csv. Last file has the total
# of G-Mean values per file
# save_to_csv(auc_importances_drop, column_names, var_to_string(auc_importances_drop, locals()),
#             filename_and_round)
# save_to_csv(gMean_importances_drop, column_names, var_to_string(gMean_importances_drop, locals()),
#             filename_and_round)
# save_to_csv(fBeta_importances_drop, column_names, var_to_string(fBeta_importances_drop, locals()),
#             filename_and_round)

# After calculating FP TP FN TN etc.:
# # Predict for validation data:
# probas_ = get_probabilities(clf, x_test, is_svm)
# y_pred = clf.predict(x_test)
#
# y_real_list.append(y_test)
# probability_list.append(probas_)
#
# # Compute ROC curve and area under the curve
# fpr, tpr, thresholds = roc_curve(y_test, probas_)
#
# tprs.append(interp(mean_fpr, fpr, tpr))
# tprs[-1][0] = 0.0
# roc_auc = auc(fpr, tpr)
# aucs.append(roc_auc)
#
# # calculate confusion matrix, precision, f1 and Matthews Correlation Coefficient
#
# tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
# precision = precision_score(y_test, y_pred)
# mcc = matthews_corrcoef(y_test, y_pred)
# f1 = f1_score(y_test, y_pred)
#
# recall = tp / (fn + tp)  # TPR/Recall
# tnr = tn / (tn + fp)  # TNR
#
# tnr_list.append(tnr)  # TNR
# tpr_list.append(recall)  # TPR/Recall
# fpr_list.append(fp / (tn + fp))  # FPR
# fnr_list.append(fn / (fn + tp))  # FNR
#
# beta = 0.5
# # fBeta = (1+beta*beta)*((precision*recall)/((beta*beta*precision)+recall))
# fBeta = fbeta_score(y_test, y_pred, beta=5)
# gMean = math.sqrt(recall * tnr)
#
# precision_list.append(precision)
# f1_list.append(f1)
# mcc_list.append(mcc)
# f_beta_list.append(fBeta)
# g_mean_list.append(gMean)

# Average the metrics over folds

####################################################################################################################
# """Show metrics"""
#
# print("TN: %.02f %% ± %.02f %% - FN: %.02f %% ± %.02f %%" % (np.mean(tnr_list),
#                                                              np.std(tnr_list),
#                                                              np.mean(fnr_list),
#                                                              np.std(fnr_list)))
# print("FP: %.02f %% ± %.02f %% - TP: %.02f %% ± %.02f %%" % (np.mean(fpr_list),
#                                                              np.std(fpr_list),
#                                                              np.mean(tpr_list),
#                                                              np.std(tpr_list)))
#
# print(
#     "Precision: %.02f %% ± %.02f %% - F1: %.02f %% ± %.02f %% - MCC: %.02f %% ± %.02f %% - fBeta: %.02f %% ± "
#     "%.02f %% - gMean: %.02f %% ± %.02f %%" % (np.mean(precision_list),
#                                                np.std(precision_list),
#                                                np.mean(f1_list),
#                                                np.std(f1_list),
#                                                np.mean(mcc_list),
#                                                np.std(mcc_list),
#                                                np.mean(f_beta_list),
#                                                np.std(f_beta_list),
#                                                np.mean(g_mean_list),
#                                                np.std(g_mean_list)
#                                                ))
#
# print("AUC: %.02f %% ± %.02f %%" % (auc_mean_percent, auc_std_percent))

# tnr_list = 100 * np.array(tnr_list)
# tpr_list = 100 * np.array(tpr_list)
# fnr_list = 100 * np.array(fnr_list)
# fpr_list = 100 * np.array(fpr_list)
# precision_list = 100 * np.array(precision_list)
# f1_list = 100 * np.array(f1_list)
# mcc_list = 100 * np.array(mcc_list)
# f_beta_list = 100 * np.array(f_beta_list)
# g_mean_list = 100 * np.array(g_mean_list)
#
# # Average the TPR over folds
#
# mean_tpr = np.mean(tprs, axis=0)
# mean_tpr[-1] = 1.0
# mean_auc = auc(mean_fpr, mean_tpr)
# std_auc = np.std(aucs)
# auc_mean_percent = 100 * mean_auc
# auc_std_percent = 100 * std_auc
