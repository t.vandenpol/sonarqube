"""
version 2.0
@author: Sergio Moreschini, Francesco Lomio

"""
########################################################################################################################
import math
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import time
import pickle
import Main.Code.settings as s
from Main.Code.classification_cross_validation import cross_validate, make_sure_folder_exists
from datetime import datetime

########################################################################################################################
from Main.Code.help_functions import is_file, f_beta, var_to_string
from Main.Code.plots import plot_importances, plot_auc_for_single_ml, plot_single_precision_recall, \
    plot_optimal_g_mean_importances, plot_optimal_f_5_importances, plot_optimal_f_1_importances


def main_option_ml(model):
    # Read in data and create the variable df to manipulate it
    df = pd.read_csv(s.path, low_memory=False)
    # Only use one project, used when s.ONE_PROJECT in settings is defined.
    if not s.one_project == "off":
        df = df[df['projectID'].str.contains(s.one_project)]
    # Remove infinite values and fill NaN values with zeros
    df = df.replace([np.inf, -np.inf], np.nan).fillna(0)
    # variables assignment
    y = df[s.target]
    y = np.array(y)

    # We want to use the difference in SonarQube measures (M_) between the last and current commit, instead of
    # using the plain SonarQube measures (M_).
    # To subtract the current commits from the last commits, we delete the last index to the subtraction values
    # and add 0 to the start of the subraction values.
    # and replace the value that is subtracted from the first commit of a project to 0.
    # We subtract xm3 (previous commits) from xm (original commits)

    # Get the columns with M_ in it
    column_names = [c for c in df.columns if "M_" in c]
    xm = df[column_names]
    xm = np.array(xm)  # xm contains the absolute values of the SonarQube measures. e.g. 1000 classes.

    # We are here working to xm3 which should contain the values of the commit before the commit in xm
    # We start by deleting the last row and setting the first row to zero.
    xm2 = xm[0:len(xm) - 1]
    xm3 = np.insert(xm2, 0, 0, axis=0)
    # Now we change the last commit of each project to zero in xm3.
    # Since we need to subtract zero from a project when it just started.
    # Get indexes of start of project. For example accumulo changes to ambari at index 2641 in xm and 2642 in xm3.
    project_col = df['projectID']
    project_col = np.array(project_col)
    # We start by deleting the last row and setting the first row to zero.
    project_col2 = project_col[0:len(project_col) - 1]
    project_col3 = np.insert(project_col2, 0, 'accumulo', axis=0)
    # Start making a list of indices where the project is not the same as the old project.
    start_index_projects = []
    old_project = 'accumulo'
    for i in range(len(project_col3)):
        project = project_col3[i]
        if project != old_project:
            start_index_projects.append(i - 1)  # To get the commit before start of this project
        old_project = project
    # Use start_index_projects to replace these indexes in xm3 with rows of zeroes.
    for i in start_index_projects:
        xm3[i] = np.zeros(np.size(xm3, 1))
    # Now subtract the previous commits (xm3) from the original rows (xm)
    diff = xm - xm3
    # Use the diff instead of xm
    xm = diff
    # Add squid to M_diff
    col_x = [c for c in df.columns if "squid" in c]
    x = df[col_x]
    # std_beniweud = x.std(0)
    # std_sorted = std_beniweud.sort_values(0, ascending=False)
    # std_sorted.to_csv('std_sorted.csv')
    x = np.array(x)

    # Add x (squid) to xm.
    xm = np.append(xm, x, axis=1)
    # Add col_x to column_names
    column_names += col_x

    # Perm plots counts
    auc_imp_count_perm = 2000
    g_mean_count_perm = 2100
    f_beta_count_perm = 2200
    auc_count_perm = 2300
    prec_recall_count_perm = 2400
    # Drop-column plots counts
    auc_imp_count_drop = 3000
    g_mean_count_drop = 3100
    f_beta_count_drop = 3200
    auc_count_drop = 3300
    prec_recall_count_drop = 3400

    for clf, name in model:
        auc_imp_count_perm += 1
        g_mean_count_perm += 1
        f_beta_count_perm += 1
        auc_count_perm += 1
        prec_recall_count_perm += 1
        auc_imp_count_drop += 1
        g_mean_count_drop += 1
        f_beta_count_drop += 1
        auc_count_drop += 1
        prec_recall_count_drop += 1

        start = time.time()  # Start measuring the time it takes to run this ML model
        print("Calculations started at ", datetime.now())  # print current time
        is_svm = 'SVM' == name  # Set is_svm variable
        print("Evaluating %s classifier (M)" % name)
        # Don't cross_validate if the calculations are already done
        filename = s.one_project + name + s.k_means_smote
        target_file = ['./baseline_raw/', './aucs/', './precision_recall_curve/']
        # 'DROP_COLUMN'|'PERMUTATION'|'BOTH'|'off'
        if s.permutation == 'on':
            target_file.append('./perm_raw/')
        if s.drop_column == 'on':
            target_file.append('./drop_raw/')
        _, n_cols = xm.shape  # get number of columns

        run_cross = False
        for i in target_file:
            filepath_and_name = i + filename + '.data'
            make_sure_folder_exists(filepath_and_name)
            if not is_file(filepath_and_name) or s.overwrite_saves == 'on' or s.rerun_aucs == 'on':  # Calculate and save the needed values is they are not yet saved
                run_cross = True

        if run_cross:
            cross_validate(clf, xm, y, column_names, name, is_svm, filename)

        # if s.feature == 'M_':
        #     m_rocs = [name, fpr_baseline, tpr_baseline, auc(fpr_baseline, tpr_baseline)]
        # if s.feature == 'squid_M_diff':
        #     squid_m_diff_rocs = [name, fpr_baseline, tpr_baseline, auc(fpr_baseline, tpr_baseline)]
        #
        # save_name = "%s_%s_%s_%s_" % (s.one_project, s.feature, s.importance_method, s.k_means_smote)
        #
        # target_file = ['./auc_raw_ML/', './auc_raw_comparison/']
        # if s.feature == 'M_':
        #     for i in target_file:
        #         make_sure_folder_exists(i)
        #         with open(i + 'M_rocs_%s.data' % name, 'wb') as fp:  # Pickling
        #             pickle.dump(m_rocs, fp)
        #
        # if s.feature == 'squid_M_diff':
        #     for i in target_file:
        #         make_sure_folder_exists(i)
        #         with open(i + save_name + 'rocs_%s.data' % name, 'wb') as fp:  # Pickling
        #             pickle.dump(squid_m_diff_rocs, fp)

        end = time.time()
        print('Total time: %s' % (end - start))  # Print the time it takes to run the ML

        # READ SAVED METRICS FOR ONLY THIS ML AND CALL SINGLE PLOT FUNCTIONS:
        print('Reading metrics single plots')
        for i in target_file:
            filepath_and_name = i + filename + '.data'
            if is_file(filepath_and_name):
                with open(filepath_and_name, 'rb') as fp:
                    if i == './baseline_raw/':
                        ml_name, tp_baseline, fn_baseline, tn_baseline, fp_baseline, roc_auc_baseline_added = pickle.load(fp)
                    elif i == './aucs/':
                        ml_name, tprs, mean_fpr, aucs, thresholds = pickle.load(fp)
                    elif i == './precision_recall_curve/':
                        ml_name, probas_pred_list, y_true_list = pickle.load(fp)
                    elif i == './perm_raw/':
                        ml_name, tp_added_perm, tn_added_perm, fp_added_perm, fn_added_perm, roc_auc_added_perm = pickle.load(fp)
                    elif i == './drop_raw/':
                        ml_name, tp_added_drop, tn_added_drop, fp_added_drop, fn_added_drop, roc_auc_added_drop = pickle.load(fp)
            else:  # Calculate the values with cross validation
                raise Exception("File " + filepath_and_name + " not found")

        # Print results
        print("Filename: " + str(filename))
        print("confusion matrix")
        print("TP: %s FN: %s" % (tp_baseline, fn_baseline))
        print("TN: %s FP: %s" % (tn_baseline, fp_baseline))

        # #INITIAL VALUES:
        # tp_added_perm = np.zeros(n_cols)  # Permutation
        # tn_added_perm = np.zeros(n_cols)
        # fp_added_perm = np.zeros(n_cols)
        # fn_added_perm = np.zeros(n_cols)
        # roc_auc_added_perm = np.zeros(n_cols)
        #
        # tp_added_drop = np.zeros(n_cols)  # Drop-Column
        # tn_added_drop = np.zeros(n_cols)
        # fp_added_drop = np.zeros(n_cols)
        # fn_added_drop = np.zeros(n_cols)
        # roc_auc_added_drop = np.zeros(n_cols)
        #
        # tp_baseline = 0  # Baseline
        # tn_baseline = 0
        # fp_baseline = 0
        # fn_baseline = 0
        # roc_auc_baseline_added = 0

        # -------------------- CALCULATE METRICS FOR BASELINE ------------------------
        # Calculate Positive and Negative ratings
        tpr_baseline = tp_baseline / (tp_baseline + fn_baseline)
        fnr_baseline = fn_baseline / (fn_baseline + tp_baseline)
        tnr_baseline = tn_baseline / (tn_baseline + fp_baseline)
        fpr_baseline = fp_baseline / (fp_baseline + tn_baseline)
        # Calculate precision and recall
        precision_baseline = tp_baseline / (tp_baseline + fp_baseline)
        recall_baseline = tpr_baseline
        # Matthews Correlation Coefficient (MCC)
        mcc_baseline = (tp_baseline * tn_baseline - fp_baseline * fn_baseline) / math.sqrt((tp_baseline + fp_baseline)
                            * (tp_baseline + fn_baseline) * (tn_baseline + fp_baseline) * (tn_baseline + fn_baseline))
        f_5_baseline = f_beta(5, precision_baseline, recall_baseline)
        g_mean_baseline = math.sqrt(tpr_baseline * tnr_baseline)
        auc_average_baseline = roc_auc_baseline_added / s.n_folds

        # -------------------- CALCULATE METRICS FOR IMPORTANCES ------------------------
        # Permutation metrics: calculate permutation metrics
        # If permutation: calculate importances G-Mean
        # Calculate Positive and Negative ratings
        # -------------------- PERMUTATION ------------------------
        print('Reading permutation metrics')
        if s.permutation == 'on':
            tpr_perm = tp_added_perm / (tp_added_perm + fn_added_perm)
            tnr_perm = tn_added_perm / (tn_added_perm + fp_added_perm)
            # fpr_perm = fp_added_perm / (fp_added_perm + tn_added_perm)
            # fnr_perm = fn_added_perm / (fn_added_perm + tp_added_perm)
            # Calculate precision and recall
            precision_perm = tp_added_perm / (tp_added_perm + fp_added_perm)
            recall_perm = tpr_perm
            # Matthews Correlation Coefficient (MCC)
            # mcc_perm = (tp_added_perm * tn_added_perm - fp_added_perm * fn_added_perm) / \
            #                math.sqrt((tp_added_perm + fp_added_perm) * (tp_baseline + fn_added_perm) * (tn_added_perm + fp_added_perm) * (
            #                        tn_added_perm + fn_added_perm))
            f_5_perm = np.zeros(n_cols)
            g_mean_perm = np.zeros(n_cols)
            # Calculate baselines - importances to get the importances scores.
            for col in range(n_cols):
                f_5_perm[col] = f_5_baseline - (f_beta(5, precision_perm[col], recall_perm[col]))
                g_mean_perm[col] = g_mean_baseline - (math.sqrt(tpr_perm[col] * tnr_perm[col]))
            auc_average_perm = roc_auc_added_perm / s.n_folds

        # -------------------- SHOW METRICS FOR BASELINE ------------------------
        print("BASELINE METRICS:")
        print("TPR: %.02f %% FNR: %.02f %%" % ((tpr_baseline * 100), (fnr_baseline * 100)))
        print("TNR: %.02f %% FPR: %.02f %%" % (tnr_baseline * 100, fpr_baseline * 100))
        print("Precision: %.02f %% - f5: %.02f %% - MCC: %.02f %% - gMean: %.02f %%" %
              (precision_baseline * 100, f_5_baseline * 100, mcc_baseline * 100, g_mean_baseline * 100))
        print("AUC: %.02f %%" % (auc_average_baseline * 100))

        # --- Print when single curves are on
        # -------------------- PLOT SINGLE AUC ------------------------
        if s.plot_single_curves == 'on':
            print('Plotting single auc')
            plt.figure(auc_count_drop)  # Single AUC
            plot_auc_for_single_ml(tprs, mean_fpr, aucs, filename, ml_name, thresholds)

            # -------------------- PLOT SINGLE PRECISION RECALL CURVE ------------------------
            print('Plotting single precision-recall curve')
            plt.figure(prec_recall_count_drop)  # Single AUC
            plot_single_precision_recall(y_true_list, probas_pred_list, filename, ml_name)


        # -------------------- PLOTTING IMPORTANCES ------------------------
        # Save the averages of permutation, when this is not already done



        if s.permutation == 'on' and s.plot_single_plots == 'on':
            print('Plotting permutation plots')
            # save_to_csv(auc_average_baseline, column_names, var_to_string(auc_average_baseline, locals()), filename)
            # save_to_csv(g_mean_baseline, column_names, var_to_string(g_mean_baseline, locals()), filename)
            # save_to_csv(f_5_baseline, column_names, var_to_string(f_5_baseline, locals()), filename)
            print('Plotting AUC importances')
            plt.figure(auc_imp_count_perm)  # auc_average_perm/plot plot
            plot_importances(var_to_string(auc_average_perm, locals()), auc_average_perm, n_cols, column_names, filename, auc_average_perm, ml_name, 'AUC')
            plt.figure(auc_imp_count_perm + 1000, figsize=(6.4, 8))  # auc_average_perm/plot plot
            plot_importances(var_to_string(auc_average_perm, locals()), auc_average_perm, n_cols, column_names, filename +'30', auc_average_perm, ml_name, 'AUC', 'on')  # also plot 30 graph
            plt.figure(g_mean_count_perm)  # gMean_average_perm/plot plot
            print('Plotting G-Mean importances')
            plot_importances(var_to_string(g_mean_perm, locals()), g_mean_perm, n_cols, column_names, filename, auc_average_perm, ml_name, 'G-Mean')
            plt.figure(f_beta_count_perm)  # fBeta_average_perm/plot plot
            print('Plotting F5 importances')
            plot_importances(var_to_string(f_5_perm, locals()), f_5_perm, n_cols, column_names, filename, auc_average_perm, ml_name, 'F-5 score')

            # -------------------- PLOT SINGLE AUC ------------------------
            print('Plotting single auc')
            plt.figure(auc_count_perm)  # Single AUC
            plot_auc_for_single_ml(tprs, mean_fpr, aucs, filename, ml_name, thresholds)

            # -------------------- PLOT SINGLE PRECISION RECALL CURVE ------------------------
            print('Plotting single precision-recall curve')
            plt.figure(prec_recall_count_perm)  # Single precision recall curve
            plot_single_precision_recall(y_true_list, probas_pred_list, filename, ml_name)

        # -------------------- DROP-COLUMN ------------------------
        print('Reading drop-column metrics')
        if s.drop_column == 'on':
            tpr_drop = tp_added_drop / (tp_added_drop + fn_added_drop)
            tnr_drop = tn_added_drop / (tn_added_drop + fp_added_drop)
            # fpr_drop = fp_added_drop / (fp_added_drop + tn_added_drop)
            # fnr_drop = fn_added_drop / (fn_added_drop + tp_added_drop)
            # Calculate precision and recall
            precision_drop = tp_added_drop / (tp_added_drop + fp_added_drop)
            recall_drop = tpr_drop
            # Matthews Correlation Coefficient (MCC)
            # mcc_drop = (tp_added_drop * tn_added_drop - fp_added_drop * fn_added_drop) / \
            #                math.sqrt((tp_added_drop + fp_added_drop) * (tp_baseline + fn_added_drop) * (tn_added_drop + fp_added_drop) * (
            #                        tn_added_drop + fn_added_drop))
            f_5_drop = np.zeros(n_cols)
            g_mean_drop = np.zeros(n_cols)
            # Calculate baselines - importances to get the importances scores.
            for col in range(n_cols):
                f_5_drop[col] = f_5_baseline - (f_beta(5, precision_drop[col], recall_drop[col]))
                g_mean_drop[col] = g_mean_baseline - (math.sqrt(tpr_drop[col] * tnr_drop[col]))
            auc_average_drop = roc_auc_added_drop / s.n_folds

        # -------------------- SHOW METRICS FOR BASELINE ------------------------
        print("BASELINE METRICS:")
        print("TPR: %.02f %% FNR: %.02f %%" % ((tpr_baseline * 100), (fnr_baseline * 100)))
        print("TNR: %.02f %% FPR: %.02f %%" % (tnr_baseline * 100, fpr_baseline * 100))
        print("Precision: %.02f %% - f5: %.02f %% - MCC: %.02f %% - gMean: %.02f %%" %
              (precision_baseline * 100, f_5_baseline * 100, mcc_baseline * 100, g_mean_baseline * 100))
        print("AUC: %.02f %%" % (auc_average_baseline * 100))

        # -------------------- PLOTTING IMPORTANCES ------------------------
        # Save the averages of drop-column, when this is not already done

        if s.drop_column == 'on' and s.plot_single_plots == 'on':
            print('Plotting drop-column plots')
            # save_to_csv(auc_average_baseline, column_names, var_to_string(auc_average_baseline, locals()), filename)
            # save_to_csv(g_mean_baseline, column_names, var_to_string(g_mean_baseline, locals()), filename)
            # save_to_csv(f_5_baseline, column_names, var_to_string(f_5_baseline, locals()), filename)
            print('Plotting AUC importances')
            plt.figure(auc_imp_count_drop)  # auc_average_drop/plot plot
            plot_importances(var_to_string(auc_average_drop, locals()), auc_average_drop, n_cols, column_names,
                             filename, auc_average_drop, ml_name, 'AUC')
            plt.figure(auc_imp_count_drop + 1000, figsize=(6.4, 8))  # auc_average_drop/plot plot
            plot_importances(var_to_string(auc_average_drop, locals()), auc_average_drop, n_cols, column_names,
                             filename + '30', auc_average_drop, ml_name, 'AUC', 'on')

            plt.figure(g_mean_count_drop)  # gMean_average_drop/plot plot
            print('Plotting G-Mean importances')
            plot_importances(var_to_string(g_mean_drop, locals()), g_mean_drop, n_cols, column_names, filename,
                             auc_average_drop, ml_name, 'G-Mean')
            plt.figure(f_beta_count_drop)  # fBeta_average_drop/plot plot
            print('Plotting F5 importances')
            plot_importances(var_to_string(f_5_drop, locals()), f_5_drop, n_cols, column_names, filename,
                             auc_average_drop, ml_name, 'F-5 score')




    # Importance plot
        # Divide importances by num folds to get the average

        # Save the averages of permutation, when this is not already done
        # if s.importance_method == 'PERMUTATION':
        #     if (not is_file(location_perm)) and (s.overwrite_perm_save != 'on'):
        #         # save_to_csv(auc_average_baseline, column_names, var_to_string(auc_average_baseline, locals()), filename)
        #         # save_to_csv(g_mean_baseline, column_names, var_to_string(g_mean_baseline, locals()), filename)
        #         # save_to_csv(f_5_baseline, column_names, var_to_string(f_5_baseline, locals()), filename)
        #
        #         plt.figure(101)  # auc_average_perm/plot plot
        #         plot_importances(var_to_string(auc_average_perm, locals()), auc_average_perm)
        #         plt.figure(102)  # gMean_average_perm/plot plot
        #         plot_importances(var_to_string(g_mean_perm, locals()), g_mean_perm)
        #         plt.figure(103)  # fBeta_average_perm/plot plot
        #         plot_importances(var_to_string(f_5_perm, locals()), f_5_perm)

        # PRECISION RECALL CURVE todo: INCORRECT ATM I THINK SO
        # plt.figure(200)
        #
        # # plt.figure(200) # swtich to figure 200 about precision recall
        # precision, recall, _ = precision_recall_curve(np.array(y_true_list), np.array(probas_pred_list))
        # lab = 'Overall AUC=%.4f' % (auc(recall, precision))
        # plt.step(recall, precision, label=lab, lw=2, color='black')
        # plt.xlabel('Recall')
        # plt.ylabel('Precision')
        # plt.legend(loc='lower left', font_size='small')
        #
        # plt.tight_layout()
        #
        # target_file = "precision_recall_curve/plots/%s_%s" % (filename, s.feature)
        # make_sure_folder_exists(target_file)
        # plt.savefig(target_file + ".png")

        # Code not relevant, old code for saving importances random

        #
        # importance_average_random = pd.DataFrame(
        #     {'Variables': sorted_column_names, 'Importance': auc_average_perm[idx]})
        # target_file = "Importances_random/values/%s_%s" % (exec_name, s.FEATURE)
        # make_sure_folder_exists(target_file)
        # importance_average_random.to_csv(target_file + ".csv", columns=['Variables', 'Importance'], sep=';', index=False)

        # GeneratePrecisionRecallPlot(name, mean_tpr2, mean_precision, auc(mean_tpr2, mean_precision)

    # Save the calculated ML stats for this ML if OneProject = off

        if s.one_project == 'off':
            # Save Baseline values etc:
            save_values = [name, auc_average_baseline, tnr_baseline, fnr_baseline, fpr_baseline, tpr_baseline, precision_baseline, mcc_baseline, f_5_baseline, g_mean_baseline]
            target_file = './metrics/'
            make_sure_folder_exists(target_file)
            with open(target_file + filename + '.data', 'wb') as fp:  # Pickling
                pickle.dump(save_values, fp)

    # print('g_mean_baseline')
    # print(g_mean_baseline)
    # print('g_mean_baseline')
    if s.plot_optimal_g_mean_importances == 'on':
        plot_optimal_g_mean_importances(n_cols, column_names)
    if s.plot_optimal_f_5_importances == 'on':
        plot_optimal_f_5_importances(n_cols, column_names)
    if s.plot_optimal_f_1_importances == 'on':
        plot_optimal_f_1_importances(n_cols, column_names)
    # confusion_matrix_tnr_mean = []
    # confusion_matrix_tnr_std = []
    # confusion_matrix_fnr_mean = []
    # confusion_matrix_fnr_std = []
    # confusion_matrix_fpr_mean = []
    # confusion_matrix_fpr_std = []
    # confusion_matrix_tpr_mean = []
    # confusion_matrix_tpr_std = []
    # confusion_matrix_precision_mean = []
    # confusion_matrix_precision_std = []
    # confusion_matrix_f1_mean = []
    # confusion_matrix_f1_std = []
    # confusion_matrix_mcc_mean = []
    # confusion_matrix_mcc_std = []
    # confusion_matrix_f_beta_mean = []
    # confusion_matrix_f_beta_std = []
    # confusion_matrix_g_mean_mean = []
    # confusion_matrix_g_mean_std = []

    # return
    # return mean_fpr, mean_tpr, mean_auc, std_auc, tprs, importances_random, p, scores, column_names
