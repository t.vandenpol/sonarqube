########################################################################################################################
import matplotlib
import multiprocessing

matplotlib.use("PDF")
num_cores = multiprocessing.cpu_count()
'''
This file to be used only in case of Classification
'''


########################################################################################################################
def logistic_regression():
    from sklearn.linear_model import LogisticRegression
    return [(LogisticRegression(penalty='l1', solver='saga', random_state=0), 'LogisticRegression')]
    # return [(LogisticRegression(penalty = "l1"), 'LogisticRegression')] -- old code of are sonarqube rules inducing
    # bugs probably at that time liblinear was default, is use saga cause it is way faster


def decision_trees():
    from sklearn.tree import DecisionTreeClassifier
    return [(DecisionTreeClassifier(random_state=0), "DecisionTrees")]


def random_forest():
    from sklearn.ensemble import RandomForestClassifier
    return [(RandomForestClassifier(n_estimators=100, n_jobs=-1, random_state=0), "RandomForest")]


def bagging():
    from sklearn.ensemble import BaggingClassifier
    return [(BaggingClassifier(n_estimators=100, n_jobs=-1, random_state=0), "Bagging")]


def extra_trees():
    from sklearn.ensemble import ExtraTreesClassifier
    return [(ExtraTreesClassifier(n_estimators=100, random_state=0), "ExtraTrees")]


def adaboost_model():  # AdaBoost classifier as implemented by 'Are SonarQube Rules Inducing Bugs'
    from sklearn.ensemble import AdaBoostClassifier
    return [(AdaBoostClassifier(n_estimators=100, random_state=0), "AdaBoost")]


def gradient_boosting():
    from sklearn.ensemble import GradientBoostingClassifier
    return [(GradientBoostingClassifier(n_estimators=100,random_state=0), "GradientBoost")]


def xgboost():  # It warns for some issues, though ML paper uses it the same way
    from xgboost import XGBClassifier
    return [(XGBClassifier(n_estimators=100, n_jobs=-1,randomstate=0), "XGBoost")]


def adac2():
    from Main.Code.adac2 import AdaCost
    return [(AdaCost(algorithm='adac2', random_state=0), "AdaC2")]


def svm_model():
    from sklearn import svm
    return [(svm.LinearSVC(random_state=0, dual=False), "SVM")]  # increased max_iter=2000 does not help


def svm_slower():
    from sklearn import svm
    return [(svm.SVC(kernel='linear', probability=False, cache_size=1000, random_state=0), "SVM")]


def all_classifiers():  # all classifiers from the paper 'Are SonarQube Rules Inducing Bugs?'
    return logistic_regression() + decision_trees() + random_forest() + bagging() + extra_trees() + adaboost_model() + \
           gradient_boosting() + xgboost()


def tests():  # Used to run tests
    return svm_model() + decision_trees()
    #svm_model() + decision_trees()
    # logistic_regression() + randomforest() + gradient_boosting() + extra_trees() + xgboost() + bagging()
    # randomforest() + extra_trees() # + logistic_regression() + adaboost_model()  + decision_trees() +
    # gradient_boosting() + xgboost() + bagging()


def experiments():  # The four best classifiers from the paper 'Are SonarQube Rules Inducing Bugs?' and our own
    return gradient_boosting() + logistic_regression() + decision_trees() + random_forest() + bagging() + \
           extra_trees() + adaboost_model() + xgboost() + svm_model()  # + svm_slower()
    # decision_trees() + adaboost_model() + gradient_boosting() + xgboost() + adac2() + svm_model()
    #gradient_boosting() + logistic_regression()
        # logistic_regression() + decision_trees() + random_forest() + bagging() + extra_trees()+ adaboost_model() +\
        # gradient_boosting() + xgboost() + svm_model() + svm_slower()
    #decision_trees() + adaboost_model() + gradient_boosting() + xgboost() + adac2() + svm_model()
# 'AllClassifiers'|'Tests'|'Experiments'|'LogisticRegression'|'DecisionTrees'|RandomForest'|'Bagging'|'ExtraTrees'|'AdaBoost'
# 'GradientBoosting'|'XGBoost'|'AdaC2'|'SVM'|'SVM_slower'

