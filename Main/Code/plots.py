import math

import matplotlib.pyplot as plt
import numpy
import numpy as np
import pandas as pd
from os import listdir
import pickle

from sklearn.metrics import auc, precision_recall_curve, confusion_matrix

from Main.Code.help_functions import make_sure_folder_exists, f_beta
import Main.Code.settings as s


########################################################################################################################


# Plot comparison plots
def find_rocs_files(model_class):
    squid_rocs_files = []
    squid_rocs = []
    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []
    m_rocs_files = []
    m_rocs = []

    path = './auc_raw_%s/' % model_class

    save_name = "%s_%s_%s_%s_" % (s.one_project, s.feature, s.importance_method, s.k_means_smote)

    for file in listdir(path):
        if file.startswith("squid_"):
            squid_rocs_files.append(file)
            squid_rocs_files
        if file.startswith(save_name):
            squid_m_diff_rocs_files.append(file)
            squid_m_diff_rocs_files
        if file.startswith("M_"):
            m_rocs_files.append(file)
            m_rocs_files

    for i in squid_rocs_files:
        with open(path + i, 'rb') as f:
            squid_rocs.append(pickle.load(f))
    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    for i in m_rocs_files:
        with open(path + i, 'rb') as f:
            m_rocs.append(pickle.load(f))

    return squid_rocs, squid_m_diff_rocs, m_rocs


def generate_comparison_plot(model_class, feature):
    if feature == 'squid':
        rocs = find_rocs_files(model_class)[0]
    elif feature == 'squid_M_diff':
        rocs = find_rocs_files(model_class)[1]
    else:
        rocs = find_rocs_files(model_class)[2]

    rocs = sorted(rocs, key=lambda x: x[-1], reverse=True)

    plt.figure(3)  # Plot all ROC curve
    plt.clf()

    for name, fpr, tpr, roc_auc in rocs:
        plt.plot(fpr, tpr, label="%s (AUC = %.2f %%)" % (name, 100 * roc_auc))

    plt.legend(loc=4)

    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC-curve (average over 10-folds)')

    save_name = "%s_%s_%s_%s_" % (s.one_project, s.feature, s.importance_method, s.k_means_smote)

    target_file = "classifier_comparisons/%s_%s.pdf" % (model_class, save_name)
    make_sure_folder_exists(target_file)
    plt.savefig(target_file, bbox_inches="tight")


# Plot individual AUC curves
def generate_plots(mean_fpr, mean_tpr, mean_auc, std_auc, tprs, importances_random, p, scores, column_names, name):
    plt.figure(1)
    plt.clf()

    plt.plot(mean_fpr, mean_tpr, color='b',
             label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)

    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ 1 std. dev.')

    plt.xlim(s.x_lim)
    plt.ylim(s.y_lim)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")

    target_file = "AUCs/%s_%s" % (name, s.feature)
    make_sure_folder_exists(target_file)
    plt.savefig(target_file + ".pdf", bbox_inches="tight")

    # Plot importances:
    plt.figure(2)
    plt.clf()

    # Divide importances by num folds to get the average

    importances_average_random = importances_random / s.n_splits

    idx = np.argsort(importances_average_random)
    sorted_column_names = list(np.array(column_names)[idx])

    importance_average_random = pd.DataFrame(
        {'Variables': sorted_column_names, 'Importance': importances_average_random[idx]})
    target_file = "Importances_random/values/%s_%s" % (name, s.feature)
    make_sure_folder_exists(target_file)
    importance_average_random.to_csv(target_file + ".csv", columns=['Variables', 'Importance'], sep=';', index=False)

    fontsize = 2 if p > 100 else 8

    plt.barh(np.arange(p), 100 * importances_average_random[idx], align='center')
    plt.yticks(np.arange(p), sorted_column_names, fontsize=fontsize)
    plt.xlabel("Feature importance (drop in score)")
    plt.title("Feature importances (baseline AUC = %.2f %%)" % \
              (100 * scores))

    plt.ylabel("<-- Less important     More important -->")

    target_file = "Importances_random/plots/%s_%s" % (name, s.feature)
    make_sure_folder_exists(target_file)
    plt.savefig(target_file + ".pdf", bbox_inches="tight")


# Generates the tables for the stats saved in Metrics/Project_off, dus als project_off, dan ist gesaved
def generate_tables():
    path = './metrics/'
    make_sure_folder_exists(path)
    rocs = []

    for file in listdir(path):
        #if file.endswith("%s.data" % s.k_means_smote):
        with open(path + file, 'rb') as f:
            row_and_file = pickle.load(f)
            row_and_file.append(file)
            rocs.append(row_and_file)


    model = []
    auc_mean = []
    auc_std = []
    confusion_matrix_tnr_mean = []
    confusion_matrix_tnr_std = []
    confusion_matrix_fnr_mean = []
    confusion_matrix_fnr_std = []
    confusion_matrix_fpr_mean = []
    confusion_matrix_fpr_std = []
    confusion_matrix_tpr_mean = []
    confusion_matrix_tpr_std = []
    confusion_matrix_precision_mean = []
    confusion_matrix_precision_std = []
    confusion_matrix_f1_mean = []
    confusion_matrix_f1_std = []
    confusion_matrix_mcc_mean = []
    confusion_matrix_mcc_std = []
    confusion_matrix_f_5_mean = []
    confusion_matrix_f_beta_std = []
    confusion_matrix_g_mean_mean = []
    confusion_matrix_g_mean_std = []
    filename = []

    for name, auc_baseline, tnr_baseline, fnr_baseline, fpr_baseline, tpr_baseline, precision_baseline, mcc_baseline, f_5_baseline, g_mean_baseline, file in rocs:
        model.append(name)
        auc_mean.append(auc_baseline)
        confusion_matrix_tnr_mean.append(tnr_baseline)
        confusion_matrix_fnr_mean.append(fnr_baseline)
        confusion_matrix_fpr_mean.append(fpr_baseline)
        confusion_matrix_tpr_mean.append(tpr_baseline)
        confusion_matrix_precision_mean.append(precision_baseline)
        confusion_matrix_mcc_mean.append(mcc_baseline)
        confusion_matrix_f_5_mean.append(f_5_baseline)
        confusion_matrix_g_mean_mean.append(g_mean_baseline)
        filename.append(file)

    full_table = pd.DataFrame({'model': model[:], 'auc_mean': auc_mean[:],
                               'confusion_matrix_tnr_mean': confusion_matrix_tnr_mean[:],
                               'confusion_matrix_fpr_mean': confusion_matrix_fpr_mean[:],
                               'confusion_matrix_tpr_mean': confusion_matrix_tpr_mean[:],
                               'confusion_matrix_fnr_mean': confusion_matrix_fnr_mean[:],
                               'confusion_matrix_precision_mean': confusion_matrix_precision_mean[:],
                               'confusion_matrix_mcc_mean': confusion_matrix_mcc_mean[:],
                               'confusion_matrix_f_5_mean': confusion_matrix_f_5_mean[:],
                               'confusion_matrix_g_mean_mean': confusion_matrix_g_mean_mean[:],
                               'filename': filename[:]})
    print('Here')
    make_sure_folder_exists(path + '/../Total_Project_off/full_table.csv')
    full_table.to_csv(path + '/../Total_Project_off/full_table.csv', index=False)


# Plot bar importances for given features
def plot_importances(folder_name, average_feature_importances, n_cols, column_names, filename, auc_average_perm, ml_name, importance_name, only_plot_30 = 'off'):
    # plot the table

    df2 = pd.read_csv(s.path2, low_memory=False)
    # difficult to accept that we do this twice (also in the save_to_csv function)

    # sort the column_names according the column_names of average_feature_importances,
    # then sort according importance values of average_feature_importances

    df3 = df2.set_index('name')

    # sort the values according to column_names
    df4 = df3.loc[column_names]
    df5 = df4.reset_index()

    if s.plot_importances_table == 'on':
        plot_importances_table(folder_name, average_feature_importances, n_cols, column_names, filename, auc_average_perm, ml_name, importance_name, only_plot_30 = 'off')


    idx = np.argsort(
        average_feature_importances)  # Gives the indices that would sort this array in increasing order
    colors = []
    for name, bug_type, severity in np.array(df5):
        # if severity == 'BLOCKER':
        #     colors.append('#a80003')
        # elif severity == 'CRITICAL':
        #     colors.append('#ff0003')
        # elif severity == 'MAJOR':
        #     colors.append('#0b802e')
        # elif severity == 'MINOR':
        #     colors.append('#0000ff')
        # elif severity == 'INFO':
        #     colors.append('#000000')
        if bug_type == 'BUG':  # bugs RED: a80003
            if severity == 'BLOCKER':
                colors.append('#a80003')
            elif severity == 'CRITICAL':
                colors.append('#c34b33')
            elif severity == 'MAJOR':
                colors.append('#db7b62')
            elif severity == 'MINOR':
                colors.append('#efa995')
            elif severity == 'INFO':
                colors.append('#ffd7cb')
        elif bug_type == 'CODE_SMELL':  # code smells GREEN: 0b802e
            if severity == 'BLOCKER':
                colors.append('#0b802e')
            elif severity == 'CRITICAL':
                colors.append('#42954c')
            elif severity == 'MAJOR':
                colors.append('#66aa6b')
            elif severity == 'MINOR':
                colors.append('#88c08a')
            elif severity == 'INFO':
                colors.append('#a9d5aa')
        elif name.startswith('M_'):  # M_sonarqube BLUE 3432a8
            colors.append('#3432a8')
        else:
            if severity == 'BLOCKER':  # Vulnerabilities PURPLE 670f8c
                colors.append('#670f8c')
            elif severity == 'CRITICAL':
                colors.append('#843fa2')
            elif severity == 'MAJOR':
                colors.append('#a165b8')
            elif severity == 'MINOR':
                colors.append('#bd8bcf')
            elif severity == 'INFO':
                colors.append('#d9b2e5')
    sorted_colors = list(np.array(colors)[idx])

    sorted_column_names = list(
        np.array(column_names)[idx])  # Gives the column names in the corresponding sorted order
    sorted_importances = average_feature_importances[idx]
    arranged_cols = np.arange(n_cols)
    if only_plot_30 =='on':
        n_plotted_features = 50
        sorted_importances = sorted_importances[-n_plotted_features:]
        sorted_colors = sorted_colors[-n_plotted_features:]
        arranged_cols = arranged_cols[-n_plotted_features:]
        sorted_column_names = sorted_column_names[-n_plotted_features:]
        plt.yticks(arranged_cols, sorted_column_names, fontsize=8)
        plt.title("Top %s %s feature importances %s" % (n_plotted_features, importance_name, ml_name))
    else:
        plt.gca().axes.get_yaxis().set_ticks([])
        plt.title("%s feature importances %s" % (importance_name, ml_name))
    plt.barh(arranged_cols, sorted_importances, align='center', color=sorted_colors)
    plt.xlabel("Feature importance (drop in score)")
    colordict = {'Bug: Blocker':'#a80003', 'Bug: Critical':'#c34b33', 'Bug: Major':'#db7b62', 'Bug: Minor':'#efa995', 'Bug: Info':'#ffd7cb', 'Vulnerabilities':'#670f8c', 'Code smell':'#0b802e', '\u0394 SonarQube metrics':'#3432a8'}
    # colordict = {'Blocker':'#a80003', 'CRITICAL': '#ff0003', 'MINOR': '#0000ff', 'Major': '#0b802e', 'Info':'#000000', 'M_sonarqube':'#3432a8'}
    colorkeys = list(colordict.keys())
    rectangles = [plt.Rectangle((0,0),1,1, color=colordict[colorkey]) for colorkey in colorkeys]
    plt.legend(rectangles, colorkeys, loc='upper left')


    plt.ylabel("<-- Less important     More important -->")

    target_file = folder_name + "/plots/%s" % filename
    make_sure_folder_exists(target_file)
    plt.savefig(target_file + ".pdf", bbox_inches="tight")

# Plot bar importances for given features
def plot_importances_table(folder_name, average_feature_importances, n_cols, column_names, filename, auc_average_perm, ml_name, importance_name, only_plot_30 = 'off'):
    # plot the table

    df2 = pd.read_csv(s.path2, low_memory=False)
    # difficult to accept that we do this twice (also in the save_to_csv function)

    # sort the column_names according the column_names of average_feature_importances,
    # then sort according importance values of average_feature_importances

    df3 = df2.set_index('name')

    # sort the values according to column_names
    df4 = df3.loc[column_names]
    df5 = df4.reset_index()

    df5['importance'] = average_feature_importances.tolist()
    df3 = df5.sort_values(by='importance', ascending=False)

    # df4 = df3.head(50)
    df4 = df3#[df3.importance > 0.2167]
    # Make sure the folder exist
    target_file = 'tables/'+filename
    make_sure_folder_exists(target_file)
    df4.to_csv(target_file+'.csv')
    # difficult to accept that we do this twice (also in the save_to_csv function)

# SOURCE: https://stackoverflow.com/questions/57708023/plotting-the-roc-curve-of-k-fold-cross-validation
def plot_auc_for_single_ml(tprs, mean_fpr, aucs, filename, name, thresholds):
    plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
             label='Chance', alpha=.8)

    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_thresholds = np.mean(thresholds, axis=0)

    # Print the best threshold e.g. the best G-Mean value
    mean_gmean = np.sqrt(mean_tpr * (1 - mean_fpr))
    max_g_mean_index = np.argmax(mean_gmean)
    print('Best threshold is %f,  with G-Mean of %.3f, mean-fpr is %f, mean-tpr is %f' % (mean_thresholds[max_g_mean_index], mean_gmean[max_g_mean_index], mean_fpr[max_g_mean_index], mean_tpr[max_g_mean_index]))
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    plt.plot(mean_fpr, mean_tpr, color='b',
             label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    # Plot the optimal G-Mean point on the graph
    plt.plot(mean_fpr[max_g_mean_index], mean_tpr[max_g_mean_index], marker='o', markersize=7, color="green",
             label='Best G-Mean point')

    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ 1 std. dev.')

    plt.xlim([-0.01, 1.01])
    plt.ylim([-0.01, 1.01])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Cross-Validation ROC ' + name)
    plt.legend(loc="lower right", prop={'size': 15})
    target_file = "AUCs/single_plots/%s" % filename
    make_sure_folder_exists(target_file)
    plt.savefig(target_file + ".pdf", bbox_inches="tight")


def plot_auc_multiple_ml():
    path = './aucs/'

    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs
    # rocs = sorted(rocs, key=lambda x: x[-1], reverse=True) # sort the resources
    plt.figure(444)  # Plot all ROC curve
    plt.clf()

    # plt.plot(mean_fpr, mean_tpr, color='b',
    #          label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
    #          lw=2, alpha=.8)

    best_g_mean = 0
    best_tpr = 0
    best_fpr = 0

    for ml_name, tprs, mean_fpr, aucs, thresholds in rocs:
        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        std_auc = np.std(aucs)
        plt.plot(mean_fpr, mean_tpr, label=r'%s (AUC = %0.2f $\pm$ %0.2f)' % (ml_name, mean_auc, std_auc))

        # Get the best threshold e.g. the best G-Mean value
        mean_thresholds = np.mean(thresholds, axis=0)
        mean_gmean = np.sqrt(mean_tpr * (1 - mean_fpr))
        max_g_mean_index = np.argmax(mean_gmean)
        print('Best threshold is %f,  with G-Mean of %.3f, mean-fpr is %f, mean-tpr is %f, ml_name is %s' % (
        mean_thresholds[max_g_mean_index], mean_gmean[max_g_mean_index], mean_fpr[max_g_mean_index],
        mean_tpr[max_g_mean_index], ml_name))
        cur_g_mean = mean_gmean[max_g_mean_index]
        if cur_g_mean > best_g_mean:
            best_g_mean = cur_g_mean
            best_tpr = mean_tpr[max_g_mean_index]
            best_fpr = mean_fpr[max_g_mean_index]

    # Plot the optimal G-Mean point on the graph
    plt.plot(best_fpr, best_tpr, marker='o', markersize=7, color="green",
             label='Best G-Mean point')

    # TODO calculate best G-Mean, nu eerst ff alle AUC herdraaien voor thresholds.

    plt.legend(loc=4)

    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC-curve (average over 10-folds)')

    save_name = s.one_project + '_auc_comparison' + s.k_means_smote

    target_file = "aucs/multi_plots/%s.pdf" % save_name
    make_sure_folder_exists(target_file)
    plt.savefig(target_file, bbox_inches="tight")


def plot_single_precision_recall(y_true_list, probas_pred_list, filename, name):
    # PRECISION RECALL CURVE todo: INCORRECT ATM I THINK SO
    plt.figure(200)

    # plt.figure(200) # swtich to figure 200 about precision recall
    precision, recall, _ = precision_recall_curve(np.array(y_true_list), np.array(probas_pred_list))
    lab = 'Overall AUC=%.4f' % (auc(recall, precision))
    plt.step(recall, precision, label=lab, lw=2, color='black')
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    # plt.legend(loc='lower left', font_size='small')
    plt.legend(loc='lower left')

    plt.title('Precision recall curve (average over 10-folds) for %s' % name)

    plt.tight_layout()

    target_file = "precision_recall_curve/single_plots/%s" % filename
    make_sure_folder_exists(target_file)
    plt.savefig(target_file + ".pdf")


def plot_multiple_precision_recall():
    path = './precision_recall_curve/'

    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs
    # rocs = sorted(rocs, key=lambda x: x[-1], reverse=True) # sort the resources
    plt.figure(511)

    best_f_5 = 0
    best_precision = 0
    best_recall = 0
    best_ml_name_5 = ''
    best_f_1 = 0
    best_precision1 = 0
    best_recall1 = 0
    best_ml_name_1 = ''

    for ml_name, probas_pred_list, y_true_list in rocs:
        # PRECISION RECALL CURVE todo: INCORRECT ATM I THINK SO
        precision, recall, thresholds = precision_recall_curve(np.array(y_true_list), np.array(probas_pred_list))
        lab = '%s (AUC=%.4f)' % (ml_name, auc(recall, precision))
        # plt.step(recall, precision, label=lab, lw=0.5)
        plt.plot(recall, precision, label=lab, lw=0.5)

        # Get the best threshold e.g. the best G-Mean value
        mean_thresholds = thresholds
        mean_f_5 = numpy.divide((1 + 5 * 5) * (np.multiply(precision, recall)), (numpy.add((5 * 5 * precision), recall)))
        mean_f_1 = numpy.divide(2 * np.multiply(precision, recall), numpy.add(precision, recall))
        # We ignore argmax since they should be zero.
        max_f_5_index = np.nanargmax(mean_f_5)
        max_f_1_index = np.nanargmax(mean_f_1)
        print('Best threshold is %f,  with F-Beta of %.3f, precision is %f, recall is %f, ml_name is %s' % (
            mean_thresholds[max_f_5_index], mean_f_5[max_f_5_index], precision[max_f_5_index],
            recall[max_f_5_index], ml_name))
        cur_f_5 = mean_f_5[max_f_5_index]
        cur_f_1 = mean_f_1[max_f_1_index]
        if cur_f_5 > best_f_5:
            best_f_5 = cur_f_5
            best_precision = precision[max_f_5_index]
            best_recall = recall[max_f_5_index]
            best_ml_name_5 = ml_name
        if cur_f_1 > best_f_1:
            best_f_1 = cur_f_1
            best_precision1 = precision[max_f_1_index]
            best_recall1 = recall[max_f_1_index]
            best_ml_name_1 = ml_name

        # Plot the optimal G-Mean point on the graph

    plt.plot(best_recall, best_precision, marker='o', markersize=7, color="green",
             label='Best F-5 point: %s (%.4f)' % (best_ml_name_5, best_f_5))
    plt.plot(best_recall1, best_precision1, marker='o', markersize=7, color="red",
             label='Best F-1 point: %s (%.4f)' % (best_ml_name_1, best_f_1))
    print('precision is %f, recall is %f, bestf_5 %f, %s' % (
        best_precision,
        best_recall, best_f_5, best_ml_name_5))
    print('precision is %f, recall is %f, bestf_1 %f, %s' % (
        best_precision1,
        best_recall1, best_f_1, best_ml_name_1))

        # plt.legend(loc='lower left', font_size='small')
    plt.xlim([0, 0.6])
    plt.ylim([0, 0.3])
    plt.legend(loc=1)
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.tight_layout()

    target_file = "precision_recall_curve/multi_plots/%s_comparison%s" % (s.one_project,  s.k_means_smote)
    make_sure_folder_exists(target_file)
    plt.savefig(target_file + ".pdf")


def plot_multiple_pol():
    path = './precision_recall_curve/'

    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs
    # rocs = sorted(rocs, key=lambda x: x[-1], reverse=True) # sort the resources
    plt.figure(511)

    best_f_5 = 0
    best_precision = 0
    best_recall = 0
    best_ml_name_5 = ''
    best_f_1 = 0
    best_precision1 = 0
    best_recall1 = 0
    best_ml_name_1 = ''

    for ml_name, probas_pred_list, y_true_list in rocs:
        # PRECISION RECALL CURVE todo: INCORRECT ATM I THINK SO
        precision, recall, thresholds = precision_recall_curve(np.array(y_true_list), np.array(probas_pred_list))
        lab = '%s (AUC=%.4f)' % (ml_name, auc(recall, precision))
        plt.step(recall, precision, label=lab, lw=0.5)
        # plt.plot(recall, precision, label=lab, lw=0.5)

        # Get the best threshold e.g. the best G-Mean value
        mean_thresholds = thresholds
        mean_f_5 = numpy.divide((1 + 5 * 5) * (np.multiply(precision, recall)), (numpy.add((5 * 5 * precision), recall)))
        mean_f_1 = numpy.divide(2 * np.multiply(precision, recall), numpy.add(precision, recall))
        # We ignore argmax since they should be zero.
        max_f_5_index = np.nanargmax(mean_f_5)
        max_f_1_index = np.nanargmax(mean_f_1)
        # print('Best threshold is %f,  with F-Beta of %.3f, precision is %f, recall is %f, ml_name is %s' % (
        #     mean_thresholds[max_f_5_index], mean_f_5[max_f_5_index], precision[max_f_5_index],
        #     recall[max_f_5_index], ml_name))
        print('Best threshold is %f,  with F-1 of %.3f, precision is %f, recall is %f, ml_name is %s' % (
            mean_thresholds[max_f_1_index], mean_f_1[max_f_1_index], precision[max_f_1_index],
            recall[max_f_1_index], ml_name))
        cur_f_5 = mean_f_5[max_f_5_index]
        cur_f_1 = mean_f_1[max_f_1_index]
        if cur_f_5 > best_f_5:
            best_f_5 = cur_f_5
            best_precision = precision[max_f_5_index]
            best_recall = recall[max_f_5_index]
            best_ml_name_5 = ml_name
        if cur_f_1 > best_f_1:
            best_f_1 = cur_f_1
            best_precision1 = precision[max_f_1_index]
            best_recall1 = recall[max_f_1_index]
            best_ml_name_1 = ml_name

        # Plot the optimal G-Mean point on the graph

    plt.plot(best_recall, best_precision, marker='o', markersize=7, color="green",
             label='Best F-5 point: %s' % best_ml_name_5)
    plt.plot(best_recall1, best_precision1, marker='o', markersize=7, color="purple",
             label='Best F-1 point: %s' % best_ml_name_1)
    print('precision is %f, recall is %f, bestf_5 %f, %s' % (
        best_precision,
        best_recall, best_f_5, best_ml_name_5))
    print('precision is %f, recall is %f, bestf_1 %f, %s' % (
        best_precision1,
        best_recall1, best_f_1, best_ml_name_1))

        # plt.legend(loc='lower left', font_size='small')
    plt.xlim([0, 0.6])
    plt.ylim([0, 0.3])
    plt.legend(loc=1)
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.tight_layout()

    target_file = "precision_recall_curve/multi_plots/%s_comparison%s" % (s.one_project,  s.k_means_smote)
    make_sure_folder_exists(target_file)
    plt.savefig(target_file + ".pdf")

def get_f_beta_g_mean_importances():
    path = './precision_recall_curve/'

    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("GradientBoost%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs

    # plt.figure(777)

    def one_or_zeros(probas_pred_list1, threshold1):
        return_array = []
        for proba in probas_pred_list1:
            if proba > threshold1:
                return_array.append(1)
            else:
                return_array.append(0)
        return np.array(return_array)

    for ml_name, probas_pred_list, y_true_list in rocs:
        print(ml_name)
        threshold = 0.005042
        y_prediction = one_or_zeros(probas_pred_list, threshold)
        tn_baseline, fp_baseline, fn_baseline, tp_baseline = confusion_matrix(y_true_list, y_prediction).ravel()
        # -------------------- CALCULATE METRICS FOR BASELINE ------------------------
        # Calculate Positive and Negative ratings
        tpr_baseline = tp_baseline / (tp_baseline + fn_baseline)
        fnr_baseline = fn_baseline / (fn_baseline + tp_baseline)
        tnr_baseline = tn_baseline / (tn_baseline + fp_baseline)
        fpr_baseline = fp_baseline / (fp_baseline + tn_baseline)
        # Calculate precision and recall
        precision_baseline = tp_baseline / (tp_baseline + fp_baseline)
        recall_baseline = tpr_baseline
        # Matthews Correlation Coefficient (MCC)
        mcc_baseline = (tp_baseline * tn_baseline - fp_baseline * fn_baseline) / math.sqrt((tp_baseline + fp_baseline)
                      * (tp_baseline + fn_baseline) * (tn_baseline + fp_baseline) * (tn_baseline + fn_baseline))
        f_5_baseline = f_beta(5, precision_baseline, recall_baseline)
        g_mean_baseline = math.sqrt(tpr_baseline * tnr_baseline)



def plot_optimal_g_mean_importances(n_cols, column_names):
    path = './probas_perm_raw/'
    # ATM hardcoded for offGRADIENTBOOSToff gmean plot # THRESHOLD OOK HARDCODED
    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("GradientBoost%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs

    # Threshold for best G-Mean GradientBoost = 0.005042
    threshold = 0.005042

    # Ik zou als ik jou was eerst de probas over die 10 averagen
    # daarna zou ik dan kijken wat je g-mean is voor elke feature. dan die allemaal returnen en comparen met baseline g-mean

    def one_or_zeros(probas_pred_list1, threshold1):
        return_array = []
        for proba in probas_pred_list1:
            if proba > threshold1:
                return_array.append(1)
            else:
                return_array.append(0)
        return np.array(return_array)

    for ml_name, y_probas_perm, y_test_perm, roc_auc_added_perm in rocs:
        print(ml_name)
        # waarom is er een element minder bij laatste ronde data voorspellen? is omdat je tien keer andere data voorspeld.
        # tien lijsten van 185 lijsten met probas voor alle datapoints, whilst y_test had je eigs maar 1 vd 185 nodig, maargoed.
        proba_list_average_per_feature = []
        round = 0
        tn_added = np.zeros(n_cols)
        fp_added = np.zeros(n_cols)
        fn_added = np.zeros(n_cols)
        tp_added = np.zeros(n_cols)
        for round_feature_list in y_probas_perm:
            #feature_proba_list_added = []
            feature = 0
            for proba_feature_datapoints in round_feature_list:
                y_prediction = one_or_zeros(proba_feature_datapoints, threshold)
                y_true_list = y_test_perm[round][0]
                tn, fp, fn, tp = confusion_matrix(y_true_list, y_prediction).ravel()
                tn_added[feature] += tn
                fp_added[feature] += fp
                fn_added[feature] += fn
                tp_added[feature] += tp
                feature += 1
            round += 1

    path = './precision_recall_curve/'

    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("GradientBoost%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs
    # rocs = sorted(rocs, key=lambda x: x[-1], reverse=True) # sort the resources
    (ml_name2, probas_pred_list2, y_true_list2) = rocs[0]
    y_prediction2 = one_or_zeros(probas_pred_list2, threshold)
    tn2, fp2, fn2, tp2 = confusion_matrix(y_true_list2, y_prediction2).ravel()
    tpr2 = tp2/(tp2+fn2)
    tnr2 = tn2/(tn2+fp2)
    g_mean_baseline = (math.sqrt(tpr2 * tnr2))
    precision2 = tp2/(tp2+fp2)
    recall2 = tpr2
    f_5_baseline = (f_beta(5, precision2, recall2))
    tp_added_perm = tp_added
    fn_added_perm = fn_added
    tn_added_perm = tn_added
    fp_added_perm = fp_added
    tpr_perm = tp_added_perm / (tp_added_perm + fn_added_perm)
    tnr_perm = tn_added_perm / (tn_added_perm + fp_added_perm)
    # fpr_perm = fp_added_perm / (fp_added_perm + tn_added_perm)
    # fnr_perm = fn_added_perm / (fn_added_perm + tp_added_perm)
    # Calculate precision and recall
    precision_perm = tp_added_perm / (tp_added_perm + fp_added_perm)
    recall_perm = tpr_perm
    # Matthews Correlation Coefficient (MCC)
    # mcc_perm = (tp_added_perm * tn_added_perm - fp_added_perm * fn_added_perm) / \
    #                math.sqrt((tp_added_perm + fp_added_perm) * (tp_baseline + fn_added_perm) * (tn_added_perm + fp_added_perm) * (
    #                        tn_added_perm + fn_added_perm))
    f_5_perm = np.zeros(n_cols)
    g_mean_perm = np.zeros(n_cols)
    # Calculate baselines - importances to get the importances scores.
    for col in range(n_cols):
        f_5_perm[col] = f_5_baseline - (f_beta(5, precision_perm[col], recall_perm[col]))
        g_mean_perm[col] = g_mean_baseline - (math.sqrt(tpr_perm[col] * tnr_perm[col]))
    g_mean_perm[col] = g_mean_baseline - (math.sqrt(tpr_perm[col] * tnr_perm[col]))
    # KLOPT NOG NIET< WAAROM IS HET MIN???
    print('Plotting G-Mean importances')
    # plot_importances(var_to_string(g_mean_perm, locals()), g_mean_perm, n_cols, column_names, filename,
    #                  auc_average_perm, ml_name, 'G-Mean')

    plt.figure(887)
    plot_importances('optimal/g_mean', g_mean_perm, n_cols, column_names, 'g_mean_gradientboost', g_mean_perm,
                     ml_name, 'G-Mean')
    plt.figure(889, figsize=(6.4, 8))
    plot_importances('optimal/g_mean', g_mean_perm, n_cols, column_names, 'g_mean50_gradientboost', g_mean_perm,
                     ml_name, 'G-Mean', 'on')
    # plt.figure(888)
    # plot_importances('optimal/f_5', f_5_perm, n_cols, column_names, 'f_5_gradientboost', f_5_perm,
    #                  ml_name, 'F-5')

def plot_optimal_f_5_importances(n_cols, column_names):
    path = './probas_perm_raw/'
    # ATM hardcoded for offRANDOMFORESToff gmean plot # THRESHOLD OOK HARDCODED
    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("RandomForest%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs

    #  threshold = 0.040027 threshold for optimal GRADIENTBOOST F1 score with k=off
    # Threshold for best F-5 for RandomForest = 0.290449 threshold = 0.020000
    threshold = 0.020000

    # Ik zou als ik jou was eerst de probas over die 10 averagen
    # daarna zou ik dan kijken wat je g-mean is voor elke feature. dan die allemaal returnen en comparen met baseline g-mean

    def one_or_zeros(probas_pred_list1, threshold1):
        return_array = []
        for proba in probas_pred_list1:
            if proba > threshold1:
                return_array.append(1)
            else:
                return_array.append(0)
        return np.array(return_array)

    for ml_name, y_probas_perm, y_test_perm, roc_auc_added_perm in rocs:
        print(ml_name)
        # waarom is er een element minder bij laatste ronde data voorspellen? is omdat je tien keer andere data voorspeld.
        # tien lijsten van 185 lijsten met probas voor alle datapoints, whilst y_test had je eigs maar 1 vd 185 nodig, maargoed.
        proba_list_average_per_feature = []
        round = 0
        tn_added = np.zeros(n_cols)
        fp_added = np.zeros(n_cols)
        fn_added = np.zeros(n_cols)
        tp_added = np.zeros(n_cols)
        for round_feature_list in y_probas_perm:
            #feature_proba_list_added = []
            feature = 0
            for proba_feature_datapoints in round_feature_list:
                y_prediction = one_or_zeros(proba_feature_datapoints, threshold)
                y_true_list = y_test_perm[round][0]
                tn, fp, fn, tp = confusion_matrix(y_true_list, y_prediction).ravel()
                tn_added[feature] += tn
                fp_added[feature] += fp
                fn_added[feature] += fn
                tp_added[feature] += tp
                feature += 1
            round += 1

    path = './precision_recall_curve/'

    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("RandomForest%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs
    # rocs = sorted(rocs, key=lambda x: x[-1], reverse=True) # sort the resources
    (ml_name2, probas_pred_list2, y_true_list2) = rocs[0]
    y_prediction2 = one_or_zeros(probas_pred_list2, threshold)
    tn2, fp2, fn2, tp2 = confusion_matrix(y_true_list2, y_prediction2).ravel()
    tpr2 = tp2/(tp2+fn2)
    tnr2 = tn2/(tn2+fp2)
    g_mean_baseline = (math.sqrt(tpr2 * tnr2))
    precision2 = tp2/(tp2+fp2)
    recall2 = tpr2
    f_5_baseline = (f_beta(5, precision2, recall2))
    tp_added_perm = tp_added
    fn_added_perm = fn_added
    tn_added_perm = tn_added
    fp_added_perm = fp_added
    tpr_perm = tp_added_perm / (tp_added_perm + fn_added_perm)
    tnr_perm = tn_added_perm / (tn_added_perm + fp_added_perm)
    # fpr_perm = fp_added_perm / (fp_added_perm + tn_added_perm)
    # fnr_perm = fn_added_perm / (fn_added_perm + tp_added_perm)
    # Calculate precision and recall
    precision_perm = tp_added_perm / (tp_added_perm + fp_added_perm)
    recall_perm = tpr_perm
    # Matthews Correlation Coefficient (MCC)
    # mcc_perm = (tp_added_perm * tn_added_perm - fp_added_perm * fn_added_perm) / \
    #                math.sqrt((tp_added_perm + fp_added_perm) * (tp_baseline + fn_added_perm) * (tn_added_perm + fp_added_perm) * (
    #                        tn_added_perm + fn_added_perm))
    f_5_perm = np.zeros(n_cols)
    g_mean_perm = np.zeros(n_cols)
    # Calculate baselines - importances to get the importances scores.
    for col in range(n_cols):
        f_5_perm[col] = f_5_baseline - (f_beta(5, precision_perm[col], recall_perm[col]))
        g_mean_perm[col] = g_mean_baseline - (math.sqrt(tpr_perm[col] * tnr_perm[col]))
    g_mean_perm[col] = g_mean_baseline - (math.sqrt(tpr_perm[col] * tnr_perm[col]))
    print('Plotting F-Beta importances')
    # plot_importances(var_to_string(g_mean_perm, locals()), g_mean_perm, n_cols, column_names, filename,
    #                  auc_average_perm, ml_name, 'G-Mean')

    # plt.figure(887)
    # plot_importances('optimal/g_mean', g_mean_perm, n_cols, column_names, 'g_mean_randomforest', g_mean_perm,
    #                  ml_name, 'G-Mean')

    plt.figure(891)
    plot_importances('optimal/f_5', f_5_perm, n_cols, column_names, 'f_5_randomforest', f_5_perm,
                     ml_name, 'F-5')
    plt.figure(892, figsize=(6.4, 8))
    plot_importances('optimal/f_5', f_5_perm, n_cols, column_names, 'f_5_50_randomforest', f_5_perm,
                     ml_name, 'F-5', 'on')

def plot_optimal_f_1_importances(n_cols, column_names):
    path = './probas_perm_raw/'
    # ATM hardcoded for offGradientBoostoff gmean plot # THRESHOLD OOK HARDCODED
    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("GradientBoost%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs

    #  threshold = 0.040027 threshold for optimal GRADIENTBOOST F1 score with k=off
    # Threshold for best F-5 for RandomForest = 0.290449 threshold = 0.020000
    threshold = 0.040027

    # Ik zou als ik jou was eerst de probas over die 10 averagen
    # daarna zou ik dan kijken wat je g-mean is voor elke feature. dan die allemaal returnen en comparen met baseline g-mean

    def one_or_zeros(probas_pred_list1, threshold1):
        return_array = []
        for proba in probas_pred_list1:
            if proba > threshold1:
                return_array.append(1)
            else:
                return_array.append(0)
        return np.array(return_array)

    for ml_name, y_probas_perm, y_test_perm, roc_auc_added_perm in rocs:
        print(ml_name)
        # waarom is er een element minder bij laatste ronde data voorspellen? is omdat je tien keer andere data voorspeld.
        # tien lijsten van 185 lijsten met probas voor alle datapoints, whilst y_test had je eigs maar 1 vd 185 nodig, maargoed.
        proba_list_average_per_feature = []
        round = 0
        tn_added = np.zeros(n_cols)
        fp_added = np.zeros(n_cols)
        fn_added = np.zeros(n_cols)
        tp_added = np.zeros(n_cols)
        for round_feature_list in y_probas_perm:
            #feature_proba_list_added = []
            feature = 0
            for proba_feature_datapoints in round_feature_list:
                y_prediction = one_or_zeros(proba_feature_datapoints, threshold)
                y_true_list = y_test_perm[round][0]
                tn, fp, fn, tp = confusion_matrix(y_true_list, y_prediction).ravel()
                tn_added[feature] += tn
                fp_added[feature] += fp
                fn_added[feature] += fn
                tp_added[feature] += tp
                feature += 1
            round += 1

    path = './precision_recall_curve/'

    squid_m_diff_rocs_files = []
    squid_m_diff_rocs = []

    for file in listdir(path):
        if file.startswith(s.one_project):
            if file.endswith("GradientBoost%s.data" % s.k_means_smote):
                squid_m_diff_rocs_files.append(file)

    for i in squid_m_diff_rocs_files:
        with open(path + i, 'rb') as f:
            squid_m_diff_rocs.append(pickle.load(f))
    rocs = squid_m_diff_rocs
    # rocs = sorted(rocs, key=lambda x: x[-1], reverse=True) # sort the resources
    (ml_name2, probas_pred_list2, y_true_list2) = rocs[0]
    y_prediction2 = one_or_zeros(probas_pred_list2, threshold)
    tn2, fp2, fn2, tp2 = confusion_matrix(y_true_list2, y_prediction2).ravel()
    tpr2 = tp2/(tp2+fn2)
    tnr2 = tn2/(tn2+fp2)
    g_mean_baseline = (math.sqrt(tpr2 * tnr2))
    precision2 = tp2/(tp2+fp2)
    recall2 = tpr2
    f_1_baseline = (f_beta(1, precision2, recall2))
    tp_added_perm = tp_added
    fn_added_perm = fn_added
    tn_added_perm = tn_added
    fp_added_perm = fp_added
    tpr_perm = tp_added_perm / (tp_added_perm + fn_added_perm)
    tnr_perm = tn_added_perm / (tn_added_perm + fp_added_perm)
    # fpr_perm = fp_added_perm / (fp_added_perm + tn_added_perm)
    # fnr_perm = fn_added_perm / (fn_added_perm + tp_added_perm)
    # Calculate precision and recall
    precision_perm = tp_added_perm / (tp_added_perm + fp_added_perm)
    recall_perm = tpr_perm
    # Matthews Correlation Coefficient (MCC)
    # mcc_perm = (tp_added_perm * tn_added_perm - fp_added_perm * fn_added_perm) / \
    #                math.sqrt((tp_added_perm + fp_added_perm) * (tp_baseline + fn_added_perm) * (tn_added_perm + fp_added_perm) * (
    #                        tn_added_perm + fn_added_perm))
    f_1_perm = np.zeros(n_cols)
    g_mean_perm = np.zeros(n_cols)
    # Calculate baselines - importances to get the importances scores.
    for col in range(n_cols):
        f_1_perm[col] = f_1_baseline - (f_beta(1, precision_perm[col], recall_perm[col]))
        g_mean_perm[col] = g_mean_baseline - (math.sqrt(tpr_perm[col] * tnr_perm[col]))
    g_mean_perm[col] = g_mean_baseline - (math.sqrt(tpr_perm[col] * tnr_perm[col]))
    print('Plotting F-Beta importances')
    # plot_importances(var_to_string(g_mean_perm, locals()), g_mean_perm, n_cols, column_names, filename,
    #                  auc_average_perm, ml_name, 'G-Mean')
    plt.figure(890)
    plot_importances('optimal/f_1', f_1_perm, n_cols, column_names, 'f_1_GradientBoost', f_1_perm,
                     ml_name, 'F-1')
    plt.figure(891, figsize=(6.4, 8))
    plot_importances('optimal/f_1', f_1_perm, n_cols, column_names, 'f_150_GradientBoost', f_1_perm,
                     ml_name, 'F-1', 'on')
