#######################################################################################################################
"""
Main Settings
"""
# -------------------- Which ML model to use (calculate or from save) ------------------------
model = 'GradientBoosting'  # Environment should be K1 to use oversampling.
# 'AllClassifiers'|'Tests'|'Experiments'|'LogisticRegression'|'DecisionTrees'|RandomForest'|'Bagging'|'ExtraTrees'|'AdaBoost'
# 'GradientBoosting'|'XGBoost'|'AdaC2'|'SVM'|'SVM_slower'|'OnlyPrint'
# 'Experiments' to define a combination of models. NOTE: AdaC2 can only run with old environment M3.3!
# -------------------- Importance method ------------------------
permutation = 'on'  # Whether to use permutation for this run 'on'|'off'
drop_column = 'off'  # Whether to use drop_column for this run
overwrite_saves = 'off'  # Whether to overwrite ml, base, auc, permutation and drop-column saves
rerun_aucs = 'off'  # used to only rerun aucs, since thresholds are not already returned in some cases
# -------------------- Oversampling method ------------------------
k_means_smote = 'on'  # 'on'|'off' use KMeansSMOTE algorithm
overwrite_k_save = 'off'  # 'on'|'off' Whether to create new oversampled data saves and overwrite saves
# -------------------- Plots and Tables ------------------------
plot_all_precision_recall = 'off'  # Whether to plot Precision Recall curves of the saved ML stats with project = 'off'
plot_all_auc = 'off'  # Whether to plot ROC curves of the saved ML stats with project = 'off'
generate_tables = 'off'  # 'on'|'off' Creates total Table for the ran MLs with one_project == 'off'
plot_single_plots = 'on'  # Whether to plat for the single MLs (only use with permutation or drop column on)
plot_single_curves = 'off'
get_f_beta_importances = 'off'
plot_thresholds = 'off'  # Used to get thresholds, use with OnlyPrint
plot_optimal_f_5_importances = 'off'  # Use with RandomForest
plot_optimal_g_mean_importances = 'off'  # Use with GradientBoosting
plot_optimal_f_1_importances = 'off'  # Use with GradientBoosting
plot_importances_table = 'off'
# Option to read only one project e.g. 'accumulo'|'commons-jxpath'|'commons-ognl'|'off'
one_project = 'off'

#######################################################################################################################
"""
Main Settings for Classic Machine Learning
"""
path = './Dataset/dataset.csv'
path2 = './Dataset/NAME_TYPE_SEVERITY2.csv'
target = 'inducing'
n_folds = 10

#######################################################################################################################
"""
Plots
"""
x_lim = [-0.05, 1.05]
y_lim = [-0.05, 1.05]
#######################################################################################################################
