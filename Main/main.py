"""
Version 2.0
Authors: Sergio Moreschini and Francesco Lomio
"""
#######################################################################################################################
# common libraries imports

# ad-hoc libraries imports
import Code.settings as s
import Code.models as models

# ad-hoc libraries imports specific for Classic ML
from Code.main_option_ml import main_option_ml

# ad-hoc libraries imports specific for Deep Learning
from Code.plots import generate_plots, generate_tables, generate_comparison_plot, find_rocs_files, plot_auc_multiple_ml, \
    plot_multiple_precision_recall, get_f_beta_g_mean_importances, plot_multiple_pol

#######################################################################################################################
# define models
import sklearn

print('The scikit-learn version is {}.'.format(sklearn.__version__))

if s.model == 'AllClassifiers':
    main_option_ml(models.all_classifiers())

if s.model == 'Tests':
    main_option_ml(models.tests())

if s.model == 'Experiments':
    main_option_ml(models.experiments())

if s.model == 'LogisticRegression':
    main_option_ml(models.logistic_regression())

if s.model == 'DecisionTrees':
    main_option_ml(models.decision_trees())

if s.model == 'RandomForest':
    main_option_ml(models.random_forest())

if s.model == 'Bagging':
    main_option_ml(models.bagging())

if s.model == 'ExtraTrees':
    main_option_ml(models.extra_trees())

if s.model == 'AdaBoost':
    main_option_ml(models.adaboost_model())

if s.model == 'GradientBoosting':
    main_option_ml(models.gradient_boosting())

if s.model == 'XGBoost':
    main_option_ml(models.xgboost())

if s.model == 'AdaC2':
    main_option_ml(models.adac2())

if s.model == 'SVM':
    main_option_ml(models.svm_model())

if s.model == 'SVM_slower':
    main_option_ml(models.svm_slower())

# Print plot comparison ML
# squid_rocs, squid_M_diff_rocs, M_rocs = find_rocs_files('comparison')

print('Saving AUC comparison plots in classifier_comparisons')
# generate_comparison_plot('comparison', s.feature)

if s.generate_tables == "on":
    generate_tables()

if s.plot_all_precision_recall == 'on':
    plot_multiple_precision_recall()
if s.plot_all_auc == 'on':
    plot_auc_multiple_ml()
if s.plot_thresholds == 'on':
    plot_multiple_pol()
if s.get_f_beta_importances == 'on':
    get_f_beta_g_mean_importances()



# if s.generate_plots == "on":
#     generate_plots(mean_fpr, mean_tpr, mean_auc, std_auc, tprs, importances_random, P, scores, column_names)

