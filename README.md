# SonarQube rule violations that actually lead to bugs

This code is inspired by the replication package of “Faults prediction based on software metrics and SonarQube rules. Machine or Deep Learning?” from Francesco Lomio, Sergio Moreschini and Valentina Lenarduzzi.
The read me in the readMe.pdf file details the instruction of this replication package.

The project is easily used in the PyCharm editor
In Settings.py the settings are explained and can be changed. The code is used for the master thesis: "SonarQube rule violations that actually lead to bugs" by Tijmen van den Pol
